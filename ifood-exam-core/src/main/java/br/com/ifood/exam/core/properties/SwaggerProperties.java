package br.com.ifood.exam.core.properties;

import java.util.Collections;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

@Configuration
@ConfigurationProperties(prefix = "ifood.exam.swagger", ignoreUnknownFields = true)
public class SwaggerProperties {
	/**
	 * API version
	 */
	private String version;
	/**
	 * API title name
	 */
	private String title;
	/**
	 * API description
	 */
	private String description;
	/**
	 * API contact url
	 */
	private String contactUrl;
	/**
	 * API contact name
	 */
	private String contactName;
	/**
	 * API contact email
	 */
	private String contactEmail;
	/**
	 * API terms of use URL
	 */
	private String termsOfServiceUrl;
	/**
	 * API license
	 */
	private String license;
	/**
	 * API license url
	 */
	private String licenseUrl;

	@Bean
	public ApiInfo apiInfo() {
		Contact contact = new Contact(contactName, contactUrl, contactEmail);
		return new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl,
				Collections.emptyList());
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactUrl() {
		return contactUrl;
	}

	public void setContactUrl(String contactUrl) {
		this.contactUrl = contactUrl;
	}

	public String getTermsOfServiceUrl() {
		return termsOfServiceUrl;
	}

	public void setTermsOfServiceUrl(String termsOfServiceUrl) {
		this.termsOfServiceUrl = termsOfServiceUrl;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getLicenseUrl() {
		return licenseUrl;
	}

	public void setLicenseUrl(String licenseUrl) {
		this.licenseUrl = licenseUrl;
	}
}