package br.com.ifood.exam.core.configuration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ifood.exam.core.properties.SwaggerProperties;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(SwaggerProperties.class)
public class SwaggerConfiguration {
	
	@Autowired
	private ApiInfo apiInfo;
	
	@Bean
	  public Docket api() {
	    return new Docket(DocumentationType.SWAGGER_2)
	        .globalResponseMessage(RequestMethod.GET, globalResponses())
	        .globalResponseMessage(RequestMethod.POST, globalResponses())
	        .globalResponseMessage(RequestMethod.DELETE, globalResponses())
	        .globalResponseMessage(RequestMethod.PATCH, globalResponses())
	        .produces(Collections.singleton(MediaType.APPLICATION_JSON_UTF8_VALUE))
	          .select()
	            .apis(RequestHandlerSelectors.basePackage("br.com.ifood.exam"))
	            .paths(PathSelectors.any())
	          .build()
	          .apiInfo(apiInfo);
	  }
	  
	  private List<ResponseMessage> globalResponses() {
	    return Arrays.asList(
	        new ResponseMessageBuilder()
	          .code(200)
	          .message(HttpStatus.OK.getReasonPhrase())
	          .build(),
	        new ResponseMessageBuilder()
	          .code(400)
	          .message(HttpStatus.BAD_REQUEST.getReasonPhrase())
	          .build(),
	        new ResponseMessageBuilder()
	          .code(500)
	          .message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
	          .build());
	  }
}