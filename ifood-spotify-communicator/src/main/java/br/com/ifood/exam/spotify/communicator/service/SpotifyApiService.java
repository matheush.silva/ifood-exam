package br.com.ifood.exam.spotify.communicator.service;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;

import br.com.ifood.exam.spotify.communicator.configuration.util.ClientCredentialsHelper;
import br.com.ifood.exam.spotify.communicator.exception.SpotifyCommunicationFailureException;
import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;
import br.com.ifood.exam.spotify.communicator.service.util.PagingUtil;

/**
 * Service layer for handling communication with Spotify SDK.
 * 
 * @author Matheus Silva.
 *
 */
@Service
public class SpotifyApiService {

	private static final Logger logger = LoggerFactory.getLogger(SpotifyApiService.class);
	
	@Autowired
	private SpotifyApi spotifyApi;

	public List<PlaylistSimplified> getPlaylistsFromSpotify(PlaylistCategory category) {
		return PagingUtil.convertPagingIntoList((getPlaylistsCategoriesTo(category)));
	}

	private Paging<PlaylistSimplified> getPlaylistsCategoriesTo(PlaylistCategory category) {
		try {
			handleToken();
			return spotifyApi.getCategorysPlaylists(category.getName())
					.country(CountryCode.BR)
					.build()
					.execute();
		} catch (SpotifyWebApiException | IOException e) {
			logger.info("Error on spotify communication " + e.getMessage());
			throw new SpotifyCommunicationFailureException(e);
		}
	}

	public List<PlaylistTrack> getPlaylistTracksFromSpotify(String playlistId) {
		return PagingUtil.convertPagingIntoList(getPlaylistTracksTo(playlistId));
	}

	private Paging<PlaylistTrack> getPlaylistTracksTo(String playlistId) {
		try {
			handleToken();
			return spotifyApi.getPlaylistsTracks(playlistId)
					.build()
					.execute();
		} catch (SpotifyWebApiException | IOException e) {
			logger.info("Error on spotify communication " + e.getMessage());
			throw new SpotifyCommunicationFailureException(e);
		}
	}
	
	private void handleToken() {
		ClientCredentialsHelper.setAccessToken(spotifyApi);
	}
}