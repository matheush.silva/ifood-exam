package br.com.ifood.exam.spotify.communicator.service.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.wrapper.spotify.model_objects.specification.ArtistSimplified;

import br.com.ifood.exam.spotify.communicator.model.Artist;

public class ArtistMapper {
	
	public static List<Artist> mapper(ArtistSimplified[] artistSimplified) {
		return Arrays.stream(artistSimplified)
					 .map(ArtistMapper::mapper)
					 .collect(Collectors.toList());
	}
	
	public static Artist mapper(ArtistSimplified artistSimplified) {
		return Artist.builder()
					.withHref(artistSimplified.getHref())
					.withName(artistSimplified.getName())
				.build();
	}
}
