package br.com.ifood.exam.spotify.communicator.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wrapper.spotify.SpotifyApi;

import br.com.ifood.exam.spotify.communicator.configuration.util.ClientCredentialsHelper;
import br.com.ifood.exam.spotify.communicator.properties.CommunicationProperties;

@Configuration
public class ApiConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(ApiConfiguration.class);
	
	@Autowired
	private CommunicationProperties properties;
	
	@Bean
	public SpotifyApi spotifyApi() {
		logger.info("Creating a new bean");
		SpotifyApi spotifyApi = new SpotifyApi.Builder()
		.setClientId(properties.getClientId())
		.setClientSecret(properties.getClientSecret())
		.build();
		ClientCredentialsHelper.setAccessToken(spotifyApi);
		return spotifyApi;
	}
}