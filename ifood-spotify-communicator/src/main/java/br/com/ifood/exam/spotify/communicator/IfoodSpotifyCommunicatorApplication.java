package br.com.ifood.exam.spotify.communicator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;

import br.com.ifood.exam.core.configuration.SwaggerConfiguration;
import br.com.ifood.exam.spotify.communicator.properties.CommunicationProperties;

@SpringBootApplication
@EnableConfigurationProperties(CommunicationProperties.class)
@EnableCaching
@Import(SwaggerConfiguration.class)
public class IfoodSpotifyCommunicatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(IfoodSpotifyCommunicatorApplication.class, args);
	}
}