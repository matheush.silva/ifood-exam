package br.com.ifood.exam.spotify.communicator.exception;

public class SpotifyCommunicationFailureException extends RuntimeException {

	private static final long serialVersionUID = -7536447766999150995L;
	
	private static final String ERROR_MESSAGE = "An error happened while trying to fetch data from Spotify`s API, please check with the Administrator.";

	public SpotifyCommunicationFailureException(Throwable e) {
		super(ERROR_MESSAGE, e);
	}
}
