package br.com.ifood.exam.spotify.communicator.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wrapper.spotify.model_objects.specification.PlaylistTrack;

import br.com.ifood.exam.spotify.communicator.model.Playlist;
import br.com.ifood.exam.spotify.communicator.model.Track;
import br.com.ifood.exam.spotify.communicator.service.util.TrackMapper;

@Service
public class PlaylistTracksService {

	@Autowired
	private SpotifyApiService spotifyApiService;

	public List<Track> getTracksFor(Playlist playlist) {
		return this.getSpotifyPlaylistTracksFor(playlist).parallelStream()
				.map(playlistTrack -> playlistTrack.getTrack())
				.filter(track -> track != null)
				.map(TrackMapper::mapper)
				.collect(Collectors.toList());
	}

	private List<PlaylistTrack> getSpotifyPlaylistTracksFor(Playlist playlist) {
		return spotifyApiService.getPlaylistTracksFromSpotify(playlist.getPlaylistId());
	}
}
