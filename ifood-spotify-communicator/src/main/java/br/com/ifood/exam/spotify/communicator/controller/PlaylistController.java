package br.com.ifood.exam.spotify.communicator.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.ifood.exam.spotify.communicator.controller.util.PlaylistCategoryConverter;
import br.com.ifood.exam.spotify.communicator.exception.SpotifyCommunicationFailureException;
import br.com.ifood.exam.spotify.communicator.model.Playlist;
import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;
import br.com.ifood.exam.spotify.communicator.service.PlaylistService;

@RestController
@RequestMapping("/api/playlist")
public class PlaylistController {

	@Autowired
	private PlaylistService playlistService;

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(SpotifyCommunicationFailureException.class)
	public void handleSpotifyCommunicationFailure() {

	}

	@GetMapping("/{category}")
	public ResponseEntity<List<Playlist>> getPlaylist(@PathVariable("category") PlaylistCategory category) {
		return new ResponseEntity<List<Playlist>>(playlistService.getPlaylistFor(category), HttpStatus.OK);
	}

	@GetMapping("/{category}/random")
	public ResponseEntity<Playlist> getRandomPlaylist(@PathVariable("category") PlaylistCategory category) {
		List<Playlist> playlists = playlistService.getPlaylistFor(category);
		return new ResponseEntity<Playlist>(playlists.get(new Random().nextInt(playlists.size())), HttpStatus.OK);
	}

	@InitBinder
	public void initializePlaylistCategoryConverter(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(PlaylistCategory.class, new PlaylistCategoryConverter());
	}
}