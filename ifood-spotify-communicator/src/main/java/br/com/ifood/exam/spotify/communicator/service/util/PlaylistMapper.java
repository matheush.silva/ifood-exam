package br.com.ifood.exam.spotify.communicator.service.util;

import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;

import br.com.ifood.exam.spotify.communicator.model.Playlist;

/**
 * Transform {@code PlaylistSimplified} to {@code Playlist}.
 * @author Matheus Silva
 *
 */
public class PlaylistMapper {

	public static Playlist mapper(PlaylistSimplified playlistSimplified) {
		return Playlist.builder()
						   .withPlaylistId(playlistSimplified.getId())
						   .withHref(playlistSimplified.getHref())
						   .withName(playlistSimplified.getName())
					   .build();
	}
}