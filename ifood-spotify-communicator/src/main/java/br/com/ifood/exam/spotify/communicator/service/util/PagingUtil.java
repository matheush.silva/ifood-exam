package br.com.ifood.exam.spotify.communicator.service.util;

import java.util.Arrays;
import java.util.List;

import com.wrapper.spotify.model_objects.specification.Paging;

public class PagingUtil {
	
	public static <T> List<T> convertPagingIntoList(Paging<T> paging) {
		return Arrays.asList(paging.getItems());
	}
}
