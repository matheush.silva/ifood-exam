package br.com.ifood.exam.spotify.communicator.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;

import br.com.ifood.exam.spotify.communicator.model.Playlist;
import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;
import br.com.ifood.exam.spotify.communicator.service.util.PlaylistMapper;

@Service
public class PlaylistService {

	private static final Logger logger = LoggerFactory.getLogger(PlaylistService.class);

	@Autowired
	private SpotifyApiService spotifyApiService;

	@Autowired
	private PlaylistTracksService playlistTracksService;

	@Cacheable(value = "playlists", unless = "(#result == null || #result.size() == 0)")
	public List<Playlist> getPlaylistFor(PlaylistCategory category) {
		logger.info("Getting playlist from Spotify!");

		List<Playlist> playlistCollection = this.getSpotifyPlaylistsFor(category).parallelStream()
				.map(PlaylistMapper::mapper).collect(Collectors.toList());
		playlistCollection.parallelStream().forEach(playlist -> {
			playlist.setTracks(this.playlistTracksService.getTracksFor(playlist));
			playlist.setCategory(category);
		});
		return playlistCollection;
	}
	
	private List<PlaylistSimplified> getSpotifyPlaylistsFor(PlaylistCategory category) {
		return spotifyApiService.getPlaylistsFromSpotify(category);
	}
}