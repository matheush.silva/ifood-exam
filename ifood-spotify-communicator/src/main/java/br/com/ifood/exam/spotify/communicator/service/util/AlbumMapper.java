package br.com.ifood.exam.spotify.communicator.service.util;

import com.wrapper.spotify.model_objects.specification.AlbumSimplified;

import br.com.ifood.exam.spotify.communicator.model.Album;

public class AlbumMapper {

	public static Album mapper(AlbumSimplified album) {
		return Album.builder()
						.withName(album.getName())
						.withHref(album.getHref())
					.build();
	}

}
