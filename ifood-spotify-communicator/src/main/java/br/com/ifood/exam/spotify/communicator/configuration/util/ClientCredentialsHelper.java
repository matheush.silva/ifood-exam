package br.com.ifood.exam.spotify.communicator.configuration.util;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;

public class ClientCredentialsHelper {

	private static final Logger logger = LoggerFactory.getLogger(ClientCredentialsHelper.class);

	private static final Duration ONE_HOUR = Duration.ofHours(1);
	
	private static ClientCredentials clientCredentials;
	
	private static Instant tokenCreationDate;
	
	@Cacheable(value = "playlistsToken")
	public static void setAccessToken(SpotifyApi spotifyApi) {
		logger.info("Checking client credentials");
		try {
			checkTokenAgeAndGenerateNewToken(spotifyApi);
		} catch (SpotifyWebApiException | IOException e) {
			logger.error("Checking client credentials error " + e);
			throw new RuntimeException(MessageFormat.format("Something went wrong while getting access token from Spotify {0}.",e.getMessage()));
		}
	}

	private static void checkTokenAgeAndGenerateNewToken(SpotifyApi spotifyApi)
			throws IOException, SpotifyWebApiException {
		Duration tokenAge = getTokenAge();
		if (tokenAge.equals(ONE_HOUR)) {
			getNewSpotifyTokenAndSetsToSpotifyApi(spotifyApi);
		}
	}

	private static Duration getTokenAge() {
		Duration tokenAge = Optional.ofNullable(tokenCreationDate)
				.map(tokenCreationDate -> Duration.between(tokenCreationDate, Instant.now()))
				.orElse(ONE_HOUR);
		logger.info("Client credentials token age " + tokenAge.toMinutes());
		return tokenAge;
	}
	
	private static void getNewSpotifyTokenAndSetsToSpotifyApi(SpotifyApi spotifyApi)
			throws IOException, SpotifyWebApiException {
		logger.info("Get new Token and sets it to SpotifyApi");
		clientCredentials = spotifyApi.clientCredentials().build().execute();
		spotifyApi.setAccessToken(clientCredentials.getAccessToken());
		logger.info(clientCredentials.getAccessToken());
		tokenCreationDate = Instant.now();
	}
}