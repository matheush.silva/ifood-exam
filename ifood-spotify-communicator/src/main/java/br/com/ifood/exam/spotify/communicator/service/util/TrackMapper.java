package br.com.ifood.exam.spotify.communicator.service.util;

import br.com.ifood.exam.spotify.communicator.model.Track;

public class TrackMapper {

	public static Track mapper(com.wrapper.spotify.model_objects.specification.Track track) {
		return Track.builder()
					.withName(track.getName())
					.withHref(track.getHref())
					.withArtist(ArtistMapper.mapper(track.getArtists()))
					.withAlbum(AlbumMapper.mapper(track.getAlbum()))
				.build();
	}
}
