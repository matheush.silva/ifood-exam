package br.com.ifood.exam.spotify.communicator.controller.util;

import java.beans.PropertyEditorSupport;

import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;

public class PlaylistCategoryConverter extends PropertyEditorSupport {
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		super.setValue(PlaylistCategory.fromValue(text));
	}
}
