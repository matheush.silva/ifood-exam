package br.com.ifood.exam.spotify.communicator.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;
import br.com.ifood.exam.spotify.communicator.service.PlaylistService;
import br.com.ifood.exam.spotify.communicator.service.util.PlaylistSample;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { PlaylistController.class }, secure = false)
public class PlaylistControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PlaylistService playlistService;

	@Test
	public void testWhenIRequestAnValidCategory_thenItShouldReturn() throws Exception {
		// Given any valid category
		PlaylistCategory category = PlaylistCategory.POP;
		when(playlistService.getPlaylistFor(category))
				.thenReturn(PlaylistSample.createMockListOfPlaylistWithCategory(category));

		// when I request it
		// then it should return
		mockMvc.perform(get("/api/playlist/{category}", category.getName()))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.[0].category", is(category.getName().toUpperCase())));
	}

	@Test
	public void testWhenIRequestAnInvalidCategory_thenItShouldReturn() throws Exception {
		// Given any invalid category
		String category = "asd";

		// when I request it
		// then it should return
		mockMvc.perform(get("/api/playlist/{category}", category)).andExpect(status().is4xxClientError());
	}

}
