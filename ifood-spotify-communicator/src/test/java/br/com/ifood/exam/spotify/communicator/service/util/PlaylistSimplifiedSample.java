package br.com.ifood.exam.spotify.communicator.service.util;

import java.util.ArrayList;
import java.util.List;

import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;

public class PlaylistSimplifiedSample {

	public static List<PlaylistSimplified> createMockListOfPlaylistSimplifiedWithSize(int size) {
		List<PlaylistSimplified> mockList = new ArrayList<>();
		for (int i = 0; i < size; i++)
			mockList.add(new PlaylistSimplified.Builder().setName("Playlist Teste " + i).build());
		return mockList;
	}
	
	public static Paging<PlaylistSimplified> createMockPagingOfPlaylistSimplifiedWithSize(int size) {
		PlaylistSimplified[] items = createMockListOfPlaylistSimplifiedWithSize(10).stream().toArray(PlaylistSimplified[]::new);
		return new Paging.Builder<PlaylistSimplified>()
						 .setItems(items)
						 .build();
	}
}