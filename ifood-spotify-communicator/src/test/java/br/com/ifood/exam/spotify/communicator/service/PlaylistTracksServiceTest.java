package br.com.ifood.exam.spotify.communicator.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;

import br.com.ifood.exam.spotify.communicator.model.Track;
import br.com.ifood.exam.spotify.communicator.service.util.PlaylistMapper;
import br.com.ifood.exam.spotify.communicator.service.util.PlaylistTracksSample;
import br.com.ifood.exam.spotify.communicator.service.util.TrackMapper;

@RunWith(SpringRunner.class)
public class PlaylistTracksServiceTest {

	@InjectMocks
	private PlaylistTracksService service;

	@Mock
	private SpotifyApiService spotifyApiService;

	@Test
	public void givenAPlaylist_thenRetrieveListOfPlaylistTracks() throws SpotifyWebApiException, IOException {
		List<PlaylistTrack> mockListOfPlaylistTrack = PlaylistTracksSample.createMockListOfPlaylistTrackWithSize(10);
		PlaylistSimplified playlistSimplified = new PlaylistSimplified.Builder().setId("fakeId").build();
		when(spotifyApiService.getPlaylistTracksFromSpotify(anyString())).thenReturn(mockListOfPlaylistTrack);

		// Given a Category
		List<Track> playlistTrack = service.getTracksFor(PlaylistMapper.mapper(playlistSimplified));
		// it should return a non null list
		assertNotNull(playlistTrack);
		// a list of same size
		assertTrue(playlistTrack.size() == mockListOfPlaylistTrack.size());
		// a list that contain any of the original list itens transformed
		assertTrue(playlistTrack.contains(getRandomElementFromMockList(mockListOfPlaylistTrack)));
	}

	private Track getRandomElementFromMockList(List<PlaylistTrack> mockListOfPlaylistTrack) {
		PlaylistTrack playlistTrack = mockListOfPlaylistTrack.get(new Random().nextInt(mockListOfPlaylistTrack.size()));
		return TrackMapper.mapper(playlistTrack.getTrack());
	}

	@Test
	public void givenAPlaylistWithAnNullTrack_thenRetrieveListOfPlaylistTracksWithoutException()
			throws SpotifyWebApiException, IOException {
		List<PlaylistTrack> mockListOfPlaylistTrack = PlaylistTracksSample
				.createMockListOfPlaylistTrackWithoutTrackAndWithSize(10);
		PlaylistSimplified playlistSimplified = new PlaylistSimplified.Builder().setId("fakeId").build();
		when(spotifyApiService.getPlaylistTracksFromSpotify(anyString())).thenReturn(mockListOfPlaylistTrack);
		// Given a Category
		List<Track> playlistTrack = service.getTracksFor(PlaylistMapper.mapper(playlistSimplified));
		// it should return a non null list
		assertNotNull(playlistTrack);
	}
}