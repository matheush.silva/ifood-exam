package br.com.ifood.exam.spotify.communicator.service.util;

import java.util.ArrayList;
import java.util.List;

import com.wrapper.spotify.model_objects.specification.AlbumSimplified;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.model_objects.specification.Track;

public class PlaylistTracksSample {
	
	public static List<PlaylistTrack> createMockListOfPlaylistTrackWithSize(int size) {
		List<PlaylistTrack> mockList = new ArrayList<>();
		for (int i = 0; i < size; i++)
			mockList.add(new PlaylistTrack.Builder().setTrack(createTrackWithName("Sample Track " + i)).build());
		return mockList;
	}
	
	public static List<PlaylistTrack> createMockListOfPlaylistTrackWithoutTrackAndWithSize(int size) {
		List<PlaylistTrack> mockList = new ArrayList<>();
		for (int i = 0; i < size; i++)
			mockList.add(new PlaylistTrack.Builder().build());
		return mockList;
	}
	
	private static Track createTrackWithName(String name) {
		return new Track.Builder()
							.setName(name)
							.setAlbum(createAlbumForTrack(name))
							.setArtists(createArtistForTrack(name))
						.build();
	}

	private static ArtistSimplified createArtistForTrack(String name) {
		return new ArtistSimplified.Builder()
								       .setHref("artistLinkFor" + name)
								       .setName("Artist Name " + name)
								   .build();
	}

	private static AlbumSimplified createAlbumForTrack(String name) {
		return new AlbumSimplified.Builder()
								  	  .setHref("albumLinkFor" + name)
								  	  .setName("Album" + name)
								  .build();
	}
	
	
}
