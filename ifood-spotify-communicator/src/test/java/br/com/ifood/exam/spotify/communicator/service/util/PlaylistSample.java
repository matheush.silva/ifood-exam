package br.com.ifood.exam.spotify.communicator.service.util;

import java.util.Arrays;
import java.util.List;

import br.com.ifood.exam.spotify.communicator.model.Playlist;
import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;

public class PlaylistSample {
	
	public static List<Playlist> createMockListOfPlaylistWithCategory(PlaylistCategory category) {
		return Arrays.asList(createMockOfPlaylistWithCategory(category));
	}
	
	public static Playlist createMockOfPlaylistWithCategory(PlaylistCategory category) {
		return Playlist.builder()
				.withCategory(category)
				.build();
	}
}
