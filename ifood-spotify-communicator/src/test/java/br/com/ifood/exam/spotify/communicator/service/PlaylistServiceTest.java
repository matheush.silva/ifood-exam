package br.com.ifood.exam.spotify.communicator.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;

import br.com.ifood.exam.spotify.communicator.model.Playlist;
import br.com.ifood.exam.spotify.communicator.model.PlaylistCategory;
import br.com.ifood.exam.spotify.communicator.model.Track;
import br.com.ifood.exam.spotify.communicator.service.util.PlaylistSimplifiedSample;
import br.com.ifood.exam.spotify.communicator.service.util.PlaylistTracksSample;
import br.com.ifood.exam.spotify.communicator.service.util.TrackMapper;

@RunWith(SpringRunner.class)
public class PlaylistServiceTest {

	@InjectMocks
	private PlaylistService service;

	@Mock
	private SpotifyApiService spotifyApiService;
	
	@Mock
	private PlaylistTracksService playlistTracksService;

	@Test
	public void givenAListOfPlaylists_thenRetrieveARandomPlaylist() throws SpotifyWebApiException, IOException {
		List<PlaylistSimplified> mockListOfPlaylistSimplified = PlaylistSimplifiedSample.createMockListOfPlaylistSimplifiedWithSize(10);
		List<PlaylistTrack> mockListOfPlaylistTrack = PlaylistTracksSample.createMockListOfPlaylistTrackWithSize(10);
		List<Track> mockListOfTracks = mockListOfPlaylistTrack.stream()
			.map(playlistTrack -> playlistTrack.getTrack())
			.map(TrackMapper::mapper)
			.collect(Collectors.toList());
		when(spotifyApiService.getPlaylistsFromSpotify(any(PlaylistCategory.class)))
			.thenReturn(mockListOfPlaylistSimplified);
		when(playlistTracksService.getTracksFor(any(Playlist.class))).thenReturn(mockListOfTracks);
		
		// Given a Category
		List<Playlist> playlistCollection = service.getPlaylistFor(PlaylistCategory.ROCK);
		// it should return
		assertNotNull(playlistCollection);
	}
}