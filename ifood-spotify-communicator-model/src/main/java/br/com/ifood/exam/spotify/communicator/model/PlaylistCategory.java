package br.com.ifood.exam.spotify.communicator.model;

import java.util.Arrays;

public enum PlaylistCategory {
	
	ROCK("rock"), CLASSICAL("classical"), PARTY("party"), POP("pop");

	private String name;

	private PlaylistCategory(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static PlaylistCategory fromValue(String value) {
		for (PlaylistCategory category : values()) {
			if (category.name.equalsIgnoreCase(value)) {
				return category;
			}
		}
		throw new IllegalArgumentException(
				"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
	}
}
