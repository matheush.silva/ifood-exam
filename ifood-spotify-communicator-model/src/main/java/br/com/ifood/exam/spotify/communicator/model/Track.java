package br.com.ifood.exam.spotify.communicator.model;

import java.util.List;
import java.util.Collections;

/**
 * Model representing the Track.
 * 
 * @author Matheus Silva
 *
 */
public class Track {

	private String name;

	private String href;

	private List<Artist> artist;

	private Album album;

	private Track(Builder builder) {
		this.name = builder.name;
		this.href = builder.href;
		this.artist = builder.artist;
		this.album = builder.album;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public List<Artist> getArtist() {
		return artist;
	}

	public void setArtist(List<Artist> artist) {
		this.artist = artist;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	/**
	 * Creates builder to build {@link Track}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link Track}.
	 */
	public static final class Builder {
		private String name;
		private String href;
		private List<Artist> artist = Collections.emptyList();
		private Album album;

		private Builder() {
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withHref(String href) {
			this.href = href;
			return this;
		}

		public Builder withArtist(List<Artist> artist) {
			this.artist = artist;
			return this;
		}

		public Builder withAlbum(Album album) {
			this.album = album;
			return this;
		}

		public Track build() {
			return new Track(this);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((album == null) ? 0 : album.hashCode());
		result = prime * result + ((artist == null) ? 0 : artist.hashCode());
		result = prime * result + ((href == null) ? 0 : href.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (album == null) {
			if (other.album != null)
				return false;
		} else if (!album.equals(other.album))
			return false;
		if (artist == null) {
			if (other.artist != null)
				return false;
		} else if (!artist.equals(other.artist))
			return false;
		if (href == null) {
			if (other.href != null)
				return false;
		} else if (!href.equals(other.href))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}