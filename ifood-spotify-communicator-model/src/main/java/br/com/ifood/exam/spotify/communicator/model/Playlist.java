package br.com.ifood.exam.spotify.communicator.model;

import java.util.Collections;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model representing a Playlist with its tracks.
 * 
 * @author Matheus Silva
 *
 */
public class Playlist extends ResourceSupport {

	private String name;

	private String href;

	private String playlistId;

	@JsonProperty
	private PlaylistCategory category;

	private List<Track> tracks;

	private Playlist(Builder builder) {
		this.name = builder.name;
		this.href = builder.href;
		this.playlistId = builder.playlistId;
		this.category = builder.category;
		this.tracks = builder.tracks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(String playlistId) {
		this.playlistId = playlistId;
	}

	public PlaylistCategory getCategory() {
		return category;
	}

	public void setCategory(PlaylistCategory category) {
		this.category = category;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	/**
	 * Creates builder to build {@link Playlist}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link Playlist}.
	 */
	public static final class Builder {
		private String name;
		private String href;
		private String playlistId;
		private PlaylistCategory category;
		private List<Track> tracks = Collections.emptyList();

		private Builder() {
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withHref(String href) {
			this.href = href;
			return this;
		}

		public Builder withPlaylistId(String playlistId) {
			this.playlistId = playlistId;
			return this;
		}

		public Builder withCategory(PlaylistCategory category) {
			this.category = category;
			return this;
		}

		public Builder withTracks(List<Track> tracks) {
			this.tracks = tracks;
			return this;
		}

		public Playlist build() {
			return new Playlist(this);
		}
	}
}