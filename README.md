# iFood exam project

This is the project created for iFood exam.
The project`s idea is to create a system that will fetch a playlist for the users based on their location and weather.

# Business rules
* If temperature (celcius) is above 30 degrees, suggest tracks for party
* In case temperature is between 15 and 30 degrees, suggest pop music tracks
* If it's a bit chilly (between 10 and 14 degrees), suggest rock music tracks
* Otherwise, if it's freezing outside, suggests classical music tracks

# How to run

TBD