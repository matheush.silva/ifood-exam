package br.com.ifood.exam.service.discovery.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class IfoodServiceDiscoveryServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(IfoodServiceDiscoveryServerApplication.class, args);
	}
}