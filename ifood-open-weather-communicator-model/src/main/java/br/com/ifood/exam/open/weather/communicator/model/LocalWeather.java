package br.com.ifood.exam.open.weather.communicator.model;

public class LocalWeather {

	private Double localTemperature;
	
	private String cityName;

	private LocalWeather(Builder builder) {
		this.localTemperature = builder.localTemperature;
		this.cityName = builder.cityName;
	}

	public Double getLocalTemperature() {
		return localTemperature;
	}

	public void setLocalTemperature(Double localTemperature) {
		this.localTemperature = localTemperature;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * Creates builder to build {@link LocalWeather}.
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link LocalWeather}.
	 */
	public static final class Builder {
		private Double localTemperature;
		private String cityName;

		private Builder() {
		}

		public Builder withLocalTemperature(Double localTemperature) {
			this.localTemperature = localTemperature;
			return this;
		}

		public Builder withCityName(String cityName) {
			this.cityName = cityName;
			return this;
		}

		public LocalWeather build() {
			return new LocalWeather(this);
		}
	}
}