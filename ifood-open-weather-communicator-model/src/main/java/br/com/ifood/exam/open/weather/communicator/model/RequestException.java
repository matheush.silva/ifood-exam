package br.com.ifood.exam.open.weather.communicator.model;

public class RequestException {

	private String timestamp;
	private long status;
	private String error;
	private String message;
	private String path;

	private RequestException(Builder builder) {
		this.timestamp = builder.timestamp;
		this.status = builder.status;
		this.error = builder.error;
		this.message = builder.message;
		this.path = builder.path;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Creates builder to build {@link RequestException}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link RequestException}.
	 */
	public static final class Builder {
		private String timestamp;
		private long status;
		private String error;
		private String message;
		private String path;

		private Builder() {
		}

		public Builder withTimestamp(String timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder withStatus(long status) {
			this.status = status;
			return this;
		}

		public Builder withError(String error) {
			this.error = error;
			return this;
		}

		public Builder withMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder withPath(String path) {
			this.path = path;
			return this;
		}

		public RequestException build() {
			return new RequestException(this);
		}
	}

}