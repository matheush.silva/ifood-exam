package br.com.ifood.exam.open.weather.communicator.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.ifood.exam.open.weather.communicator.exception.OpenWeatherBadRequestException;
import br.com.ifood.exam.open.weather.communicator.service.OpenWeatherApiService;
import br.com.ifood.exam.open.weather.communicator.service.OpenWeatherResultSample;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { LocalWeatherController.class }, secure = false)
public class LocalWeatherControllerTest {

	private static final String ANY_CITY = "Any City";

	private static final String CAMPINAS = "Campinas";

	private static final String INVALID_CITY = "Invalid City";

	private static final String ERROR_MESSAGE = "Invalid parameter on request. Check it again.";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private OpenWeatherApiService openWeatherApiService;

	@Test
	public void testWhenIRequestAValidCity_thenItShouldReturn() throws Exception {
		when(openWeatherApiService.getWeatherFor(CAMPINAS))
				.thenReturn(OpenWeatherResultSample.createMockWithName(CAMPINAS));
		
		// When I request it
		// then it should return
		mockMvc.perform(get("/api/weather/{city}", CAMPINAS))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.cityName", is(CAMPINAS)))
			.andExpect(jsonPath("$.localTemperature", is(20.0)));
	}

	@Test
	public void testWhenIRequestAnInvalidCity_thenItShouldReturn() throws Exception {
		when(openWeatherApiService.getWeatherFor(anyString())).thenThrow(new OpenWeatherBadRequestException());
		
		mockMvc.perform(get("/api/weather/{city}", INVALID_CITY))
			.andExpect(status().is4xxClientError())
			.andExpect(jsonPath("$.message", is(ERROR_MESSAGE)));
	}

	@Test
	public void testWhenIRequestAValidLatLon_thenItShouldReturn() throws Exception {
		when(openWeatherApiService.getWeatherFor(10.0, 10.0))
		.thenReturn(OpenWeatherResultSample.createMockWithLatLon(10.0, 10.0));
		
		mockMvc.perform(get("/api/weather")
					.param("lat", "10.0")
					.param("lon", "10.0"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.cityName", is(ANY_CITY)))
			.andExpect(jsonPath("$.localTemperature", is(20.0)));
	}

	@Test
	public void testWhenIRequestAnInvalidLatLon_thenItShouldReturn() throws Exception {
		when(openWeatherApiService.getWeatherFor(anyDouble(), anyDouble())).thenThrow(new OpenWeatherBadRequestException());
		
		mockMvc.perform(get("/api/weather")
				.param("lat", "1000000.0")
				.param("lon", "1000000.0"))
			.andExpect(status().is4xxClientError())
			.andExpect(jsonPath("$.message", is(ERROR_MESSAGE)));
	}
}
