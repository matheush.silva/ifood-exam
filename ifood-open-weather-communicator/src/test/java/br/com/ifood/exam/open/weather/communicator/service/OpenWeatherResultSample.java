package br.com.ifood.exam.open.weather.communicator.service;

import br.com.ifood.exam.open.weather.communicator.result.model.Coord;
import br.com.ifood.exam.open.weather.communicator.result.model.Main;
import br.com.ifood.exam.open.weather.communicator.result.model.OpenWeatherResult;

public class OpenWeatherResultSample {
	
	public static OpenWeatherResult createMockWithName(String name) {
		OpenWeatherResult openWeatherResult = new OpenWeatherResult();
		openWeatherResult.setName(name);
		openWeatherResult.setCoord(createCoord(10.0, 10.0));
		openWeatherResult.setMain(createMain());
		return openWeatherResult;
	}

	public static OpenWeatherResult createMockWithLatLon(Double lat, Double lon) {
		OpenWeatherResult openWeatherResult = new OpenWeatherResult();
		openWeatherResult.setName("Any City");
		openWeatherResult.setCoord(createCoord(lat, lon));
		openWeatherResult.setMain(createMain());
		return openWeatherResult;
	}

	public static Coord createCoord(Double lat, Double lon) {
		Coord coord = new Coord();
		coord.setLat(lat);
		coord.setLon(lon);
		return coord;
	}
	
	private static Main createMain() {
		Main main = new Main();
		main.setTemp(20.0);
		return main;
	}
}
