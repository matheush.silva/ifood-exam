package br.com.ifood.exam.open.weather.communicator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ifood.exam.open.weather.communicator.result.model.OpenWeatherResult;
import br.com.ifood.exam.open.weather.communicator.service.util.UriHelper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpenWeatherApiServiceTest {

	private static final String FAKE_URI = "fakeUri";

	private static final String CITY_NAME = "fakeCity";

	@InjectMocks
	private OpenWeatherApiService service;
	
	@Mock
	private UriHelper uriHelper;
	
	@Mock
	private RestTemplateService restTemplateService;
	
	@Test
	public void whenIQueryForCity_thenItShouldReturn() {
		when(uriHelper.createUriStringForCity(anyString())).thenReturn(FAKE_URI);
		OpenWeatherResult expected = OpenWeatherResultSample.createMockWithName(CITY_NAME);
		when(restTemplateService.get(FAKE_URI, OpenWeatherResult.class)).thenReturn(expected);
		// When I query for City
		OpenWeatherResult actual = service.getWeatherFor(CITY_NAME);
		// Then it should return
		assertEquals(expected, actual);
		assertNotNull(actual.getName());
	}
	
	@Test
	public void whenIQueryForLatAndLon_thenItShouldReturn() {
		when(uriHelper.createUriStringForLatLon(anyDouble(), anyDouble())).thenReturn(FAKE_URI);
		Double lat = 10000.0;
		Double lon = 10000.0;
		OpenWeatherResult expected = OpenWeatherResultSample.createMockWithLatLon(lat, lon);
		when(restTemplateService.get(FAKE_URI, OpenWeatherResult.class)).thenReturn(expected);
		// When I query for City
		OpenWeatherResult actual = service.getWeatherFor(lat, lon);
		// Then it should return
		assertEquals(expected, actual);
		assertNotNull(actual.getCoord());
	}
}