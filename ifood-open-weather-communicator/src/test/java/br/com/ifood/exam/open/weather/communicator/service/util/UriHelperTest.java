package br.com.ifood.exam.open.weather.communicator.service.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.ifood.exam.open.weather.communicator.properties.OpenWeatherProperties;

@RunWith(SpringRunner.class)
public class UriHelperTest {

	private static final String FAKE_APP_KEY = "fakeAppKey";

	private static final String FAKE_OPEN_WEATHER_URL = "http://fakeOpenWeatherUri.com";

	private static final String BASE_URL = FAKE_OPEN_WEATHER_URL + "?appid=" + FAKE_APP_KEY + "&units=metric";

	@InjectMocks
	private UriHelper uriHelper;

	@Mock
	private OpenWeatherProperties properties;

	@Test
	public void givenACity_thenItShouldReturn() {
		when(properties.getBaseUrl()).thenReturn(FAKE_OPEN_WEATHER_URL);
		when(properties.getApiKey()).thenReturn(FAKE_APP_KEY);
		// Given a City
		String city = "FakeCity";
		// when calling createUriForCity
		String actual = uriHelper.createUriStringForCity(city);
		// then it should be
		String expected = BASE_URL + "&q=" + city;
		assertEquals(expected, actual);
		assertNotNull(actual);
	}

	@Test
	public void givenLatAndLon_thenItShouldReturn() {
		when(properties.getBaseUrl()).thenReturn(FAKE_OPEN_WEATHER_URL);
		when(properties.getApiKey()).thenReturn(FAKE_APP_KEY);
		// Given a Lat and Lon
		Double lat = 10000.0;
		Double lon = 10000.0;
		// when calling createUriForCity
		String actual = uriHelper.createUriStringForLatLon(lat, lon);
		// then it should be
		String expected = BASE_URL + "&lat=" + lat + "&lon=" + lon;
		assertEquals(expected, actual);
		assertNotNull(actual);
	}
}
