package br.com.ifood.exam.open.weather.communicator.exception;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class OpenWeatherBadRequestException extends RuntimeException {

	private static final long serialVersionUID = 3585408862357855162L;
	
	private static final String ERROR_MESSAGE = "Invalid parameter on request. Check it again.";

	public OpenWeatherBadRequestException() {
		super(ERROR_MESSAGE);
	}

	@JsonManagedReference
	@Override
	public synchronized Throwable getCause() {
		return super.getCause();
	}
}