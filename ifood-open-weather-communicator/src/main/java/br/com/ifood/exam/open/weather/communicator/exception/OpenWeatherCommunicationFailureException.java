package br.com.ifood.exam.open.weather.communicator.exception;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class OpenWeatherCommunicationFailureException extends RuntimeException {

	private static final long serialVersionUID = -3901419885733071697L;
	
	private static final String ERROR_MESSAGE = "An error happened while trying to fetch data from Open Weather`s API, please check with the Administrator.";

	public OpenWeatherCommunicationFailureException() {
		super(ERROR_MESSAGE);
	}
	
	@JsonManagedReference
	@Override
	public synchronized Throwable getCause() {
		return super.getCause();
	}
}
