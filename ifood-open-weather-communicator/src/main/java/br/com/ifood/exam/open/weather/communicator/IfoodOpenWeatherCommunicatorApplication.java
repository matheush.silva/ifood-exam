package br.com.ifood.exam.open.weather.communicator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;

import br.com.ifood.exam.core.configuration.SwaggerConfiguration;
import br.com.ifood.exam.open.weather.communicator.properties.OpenWeatherProperties;

@SpringBootApplication
@EnableConfigurationProperties(OpenWeatherProperties.class)
@EnableCaching
@Import(SwaggerConfiguration.class)
public class IfoodOpenWeatherCommunicatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(IfoodOpenWeatherCommunicatorApplication.class, args);
	}
}
