package br.com.ifood.exam.open.weather.communicator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.com.ifood.exam.open.weather.communicator.result.model.OpenWeatherResult;
import br.com.ifood.exam.open.weather.communicator.service.util.UriHelper;

@Service
public class OpenWeatherApiService {

	@Autowired
	private RestTemplateService restTemplateService;
	
	@Autowired
	private UriHelper uriHelper;

	public OpenWeatherResult getWeatherFor(String city) {
		return requestOpenWeather(uriHelper.createUriStringForCity(city));
	}

	public OpenWeatherResult getWeatherFor(Double lat, Double lon) {
		return requestOpenWeather(uriHelper.createUriStringForLatLon(lat, lon));
	}

	@Cacheable(cacheNames = { "weather" })
	private OpenWeatherResult requestOpenWeather(String uriString) {
		return restTemplateService.get(uriString, OpenWeatherResult.class);
	}
}