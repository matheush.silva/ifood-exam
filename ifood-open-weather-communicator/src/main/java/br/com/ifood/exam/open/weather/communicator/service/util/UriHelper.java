package br.com.ifood.exam.open.weather.communicator.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ifood.exam.open.weather.communicator.properties.OpenWeatherProperties;

@Component
public class UriHelper {
	
	private static final String METRIC = "metric";
	
	@Autowired
	private OpenWeatherProperties weatherProperties;
	
	public String createUriStringForCity(String city) {
		return createUriString()
				.queryParam("q", city)
				.toUriString();
	}

	public String createUriStringForLatLon(Double lat, Double lon) {
		return createUriString()
				.queryParam("lat", lat)
				.queryParam("lon", lon)
				.toUriString();
	}

	private UriComponentsBuilder createUriString() {
		return UriComponentsBuilder.fromHttpUrl(weatherProperties.getBaseUrl())
				.queryParam("appid", weatherProperties.getApiKey())
				.queryParam("units", METRIC);
	}
}