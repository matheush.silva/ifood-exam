package br.com.ifood.exam.open.weather.communicator.service.util;

import br.com.ifood.exam.open.weather.communicator.model.LocalWeather;
import br.com.ifood.exam.open.weather.communicator.result.model.OpenWeatherResult;

public class LocalWeatherMapper {

	public static LocalWeather mapper(OpenWeatherResult openWeatherResult) {
		return LocalWeather.builder()
				.withCityName(openWeatherResult.getName())
				.withLocalTemperature(openWeatherResult.getMain().getTemp())
				.build();
	}
}
