package br.com.ifood.exam.open.weather.communicator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateService {

	@Autowired
	private RestTemplate restTemplate;
	
	public <T> T get(String uriString, Class<T> clazz) {
		return this.restTemplate.exchange(uriString, HttpMethod.GET, createHttpEntity(), clazz).getBody();
	}
	
	private HttpEntity<?> createHttpEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> entity = new HttpEntity<>(headers);
		return entity;
	}
}
