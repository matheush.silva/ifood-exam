package br.com.ifood.exam.open.weather.communicator.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class CacheConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(CacheConfiguration.class);
	
	@CacheEvict(allEntries = true, cacheNames = {"weather"})
	@Scheduled(fixedDelay = 300000)
	public void cacheEvictionEveryFiveMinutes() {
		logger.info("Clearing weather cache");
	}
}