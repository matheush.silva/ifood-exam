package br.com.ifood.exam.open.weather.communicator.controller;

import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.ifood.exam.open.weather.communicator.exception.OpenWeatherBadRequestException;
import br.com.ifood.exam.open.weather.communicator.exception.OpenWeatherCommunicationFailureException;
import br.com.ifood.exam.open.weather.communicator.model.LocalWeather;
import br.com.ifood.exam.open.weather.communicator.model.RequestException;
import br.com.ifood.exam.open.weather.communicator.service.OpenWeatherApiService;
import br.com.ifood.exam.open.weather.communicator.service.util.LocalWeatherMapper;

@RestController
@RequestMapping("/api/weather")
public class LocalWeatherController {

	@Autowired
	private OpenWeatherApiService openWeatherApiService;

	@GetMapping("/{city}")
	public ResponseEntity<LocalWeather> getWeatherForCity(@PathVariable("city") String city) {
		LocalWeather localWeather = LocalWeatherMapper.mapper(openWeatherApiService.getWeatherFor(city));
		return new ResponseEntity<LocalWeather>(localWeather, HttpStatus.OK);
	}
	
	@GetMapping(params = {"lat", "lon"})
	public ResponseEntity<LocalWeather> getWeatherForLatLon(@RequestParam("lat") Double lat, @RequestParam("lon") Double lon) {
		LocalWeather localWeather = LocalWeatherMapper.mapper(openWeatherApiService.getWeatherFor(lat, lon));
		return new ResponseEntity<LocalWeather>(localWeather, HttpStatus.OK);
	}
	
	
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({OpenWeatherCommunicationFailureException.class})
	public RequestException handleOpenWeatherCommunicationFailureException(Exception e, HttpServletRequest request) {
		return createRequestException(HttpStatus.INTERNAL_SERVER_ERROR, e, request);
				
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(OpenWeatherBadRequestException.class)
	public RequestException handleOpenWeatherBadRequestException(Exception e, HttpServletRequest request) {
		return createRequestException(HttpStatus.BAD_REQUEST, e, request);
				
	}
	
	private RequestException createRequestException(HttpStatus httpStatus, Exception e,
			HttpServletRequest request) {
		return RequestException.builder()
				.withError(httpStatus.getReasonPhrase())
				.withMessage(e.getMessage())
				.withStatus(httpStatus.value())
				.withTimestamp(LocalDate.now().toString())
				.withPath(request.getRequestURI())
				.build();
	}
}