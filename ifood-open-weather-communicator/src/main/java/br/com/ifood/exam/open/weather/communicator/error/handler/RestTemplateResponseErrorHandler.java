package br.com.ifood.exam.open.weather.communicator.error.handler;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import br.com.ifood.exam.open.weather.communicator.exception.OpenWeatherBadRequestException;
import br.com.ifood.exam.open.weather.communicator.exception.OpenWeatherCommunicationFailureException;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		Series responseHttpStatusSeries = response.getStatusCode().series();
		return (HttpStatus.Series.CLIENT_ERROR.equals(responseHttpStatusSeries)
				|| HttpStatus.Series.SERVER_ERROR.equals(responseHttpStatusSeries));
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		Series responseHttpStatusSeries = response.getStatusCode().series();
		getException(responseHttpStatusSeries);
	}

	private void getException(Series responseHttpStatusSeries) {
		switch (responseHttpStatusSeries) {
			case SERVER_ERROR:
				throw new OpenWeatherCommunicationFailureException();
			case CLIENT_ERROR:
				throw new OpenWeatherBadRequestException();
			default:
				break;
		}
	}
}